
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkNavbar extends SkElement {

    get cnSuffix() {
        return 'navbar';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

    get panelTplPath() {
        return this.getAttribute('panel-tpl-path');
    }

    set panelTplPath(tplPath) {
        return this.setAttribute('panel-tpl-path', tplPath);
    }

    open() {
        this.impl.open();
    }
    close() {
        this.impl.close();
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'open') {
            if (newValue !== null && (! oldValue)) {
                this.open();
            } else {
                if (newValue === null) {
                    this.close();
                }
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    static get observedAttributes() {
        return ['open'];
    }



}
