

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkNavbarImpl extends SkComponentImpl {

    get suffix() {
        return 'navbar';
    }

    get panelEl() {
        if (! this._panelEl) {
            this._panelEl = this.comp.el.querySelector('.sk-navbar-panel');
        }
        return this._panelEl;
    }

    beforeRendered() {
        super.beforeRendered();
        this.templates = this.comp.querySelectorAll('template');
        this.saveState();
    }

    checkForAutoOpen(event) {
        if (this.comp.getAttribute('align') === 'right') {
            if ((event.offsetX + (event.offsetX * 0.2)) > window.innerWidth) {
                this.open();
            } else {
                this.close();
            }
        } else if (this.comp.getAttribute('align') === 'top') {
            if ((event.offsetY + (event.offsetY * 0.2) - this.getScrollTop()) < (window.innerHeight / 7)) {
                this.open();
            } else {
                this.close();
            }
        } else if (this.comp.getAttribute('align') === 'bottom') {
            if ((event.offsetY + (event.offsetY * 0.4) - this.getScrollTop()) > window.innerHeight) {
                this.open();
            } else {
                this.close();
            }
        } else {
            if ((event.offsetX + (event.offsetX * 0.2)) < (window.innerWidth / 7)) {
                this.open();
            } else {
                this.close();
            }
        }
    }

    afterRendered() {
        super.afterRendered();
        if (this.templates) {
            for (let tpl of this.templates) {
                this.comp.appendChild(tpl);
            }
        }
        if (this.comp.hasAttribute('auto-open')) {
            if (this.autoOpenHandler) {
                document.removeEventListener('click', this.autoOpenHandler);
            }
            this.autoOpenHandler = document.addEventListener('mousemove', function (event) {
                this.checkForAutoOpen(event);
            }.bind(this));
        }
    }

    async renderPanelTpl() {
        if (this.comp.panelTplPath) {
            this.comp.panelTpl = await this.comp.renderer.mountTemplate(this.comp.panelTplPath, this.comp.constructor.name + 'PanelTpl', // :TODO make cachedTplId better
                this.comp, {
                    themePath: `${this.comp.basePath}/theme/${this.comp.theme}`
                });
            this.renderWithVars(this.getOrGenId(), this.panelEl, this.comp.panelTpl);
        }
    }

    bindSticky(targetEl, index) {
        if (!targetEl) {
            return false;
        }
        this.stickyHandler = this.stickyHandler ? this.stickyHandler : [];
        if (this.stickyHandler && this.stickyHandler[index]) {
            window.removeEventListener('scroll', this.stickyHandler[index]);
        }
        this.stickyHandler[index] = function(event) {
            let scrollTop = this.getScrollTop();
            if (scrollTop > 0) {
                targetEl.style.position = 'fixed';
            } else {
                targetEl.style.removeProperty('position');
            }
        }.bind(this);
        window.addEventListener('scroll', this.stickyHandler[index]);
    }

    onDisconnected() {
        if (this.stickyHandler) {
            for (let handlerKey of Object.keys(this.stickyHandler)) {
                let handler = this.stickyHandler[handlerKey];
                window.removeEventListener('scroll', handler);
            }
        }
    }

    setAlignStyling() {
        let align = this.comp.getAttribute('align');
        if (align) {
            this.bodyEl.classList.add('sk-navbar-' + align);
            if (this.comp.hasAttribute('panel')) {
                this.panelEl.classList.add('sk-navbar-panel-' + align);
            }
            this.setContentLength(align);
        } else {
            this.bodyEl.classList.add('sk-navbar-left');
            if (this.comp.hasAttribute('panel')) {
                this.panelEl.classList.add('sk-navbar-panel-left');
            }
        }
    }

    setContentLength(align) {
        if (this.comp.hasAttribute('clength')) {
            if (align === 'left' || align === 'right' || !align) {
                this.bodyEl.style.width = this.comp.getAttribute('clength');
            } else {
                this.bodyEl.style.height = this.comp.getAttribute('clength');
            }
        }
    }

}